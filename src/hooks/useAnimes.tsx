import { useEffect, useState } from "react";

import useLocalStorage from "./useLocalStorage";

import IAnimes from "../interfaces/IAnimes";
import { Location, NavigateFunction, useLocation, useNavigate, useSearchParams } from "react-router-dom";

interface IUseAnimes {
    state: {
        searchedAnimes: IAnimes[];
        params: URLSearchParams,
        allAnimes: number;
        inputValue: string;
        completedAnimes: number;
        loading: boolean;
        error: boolean;
        alert: boolean;
        modalShow: boolean;
    }
    stateUpdaters: {
        navigate: NavigateFunction;
        location: Location;
        setInputValue: React.Dispatch<React.SetStateAction<string>>;
        handleChangeInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
        handleChangeAnimeStatus: (id: number) => void;
        handleAddAnime: (animeName: string) => void;
        handleGetAnime: (id: number) => IAnimes | undefined;
        handleEditAnime: (id: number, value: string) => void;
        handleDeleteAnime: (id: number) => void;
        setModalShow: React.Dispatch<React.SetStateAction<boolean>>;
        sincronizeAnimes: () => void;
    }
}


const useAnimes = (): IUseAnimes => {

    const navigate = useNavigate();
    const location = useLocation();

    const [params] = useSearchParams();

    const [modalShow, setModalShow] = useState<boolean>(false);

    const {
        item: animes,
        saveItem: handleSaveAnimes,
        sincronizeItem: sincronizeAnimes,
        loading,
        error,
    } = useLocalStorage<IAnimes[]>('ANIMES_V2', []);

    const [inputValue, setInputValue] = useState<string>(params.get('find')?.toString() ?? '');

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setInputValue(event.target.value)
    }

    const completedAnimes: number = animes.filter(anime => anime.completed).length;
    const allAnimes: number = animes.length;

    const searchedAnimes = animes.filter(anime => anime.text.toLowerCase().includes(inputValue.toLowerCase())).sort((a, b) => (a.completed === b.completed) ? 0 : a.completed ? 1 : -1);


    const handleAddAnime = (animeName: string) => {

        const id: number = newTodoId(animes);

        if (animes.some(anime => anime.text.toLowerCase() === animeName.toLowerCase())) {
            return;
        } else if (animeName.trim() === '') {
            return;
        }
        const newAnimes = [...animes];
        newAnimes.push({
            id: id,
            text: animeName,
            completed: false,
        })
        handleSaveAnimes(newAnimes);
    }

    const handleGetAnime = (id: number): IAnimes | undefined => {
        const anime = animes.find(anime => anime.id === id)
        return (anime)
    }

    const handleChangeAnimeStatus = (id: number): void => {
        const newAnimes = [...animes];
        const animeIndex = newAnimes.findIndex(
            (anime) => anime.id === id
        )
        newAnimes[animeIndex].completed = !newAnimes[animeIndex].completed;
        handleSaveAnimes(newAnimes);
    }

    const handleEditAnime = (id: number, newAnimeName: string): void => {
        const newAnimes = [...animes];
        const animeIndex = newAnimes.findIndex(
            (anime) => anime.id === id
        )
        newAnimes[animeIndex].text = newAnimeName;
        handleSaveAnimes(newAnimes);
    }

    const handleDeleteAnime = (id: number): void => {
        const newAnimes = [...animes];
        const animeIndex = newAnimes.findIndex(
            (anime) => anime.id === id
        )
        newAnimes.splice(animeIndex, 1)
        handleSaveAnimes(newAnimes);
    }

    useEffect(() => {
        allAnimes > 0 && completedAnimes === allAnimes ? setAlert(true) : setAlert(false)
    }, [allAnimes, completedAnimes]);

    const [alert, setAlert] = useState<boolean>(false);

    const state = {
        params,
        searchedAnimes,
        allAnimes,
        inputValue,
        completedAnimes,
        loading,
        error,
        alert,
        modalShow,
    }

    const stateUpdaters = {
        setInputValue,
        navigate,
        location,
        handleChangeInput,
        handleChangeAnimeStatus,
        handleAddAnime,
        handleGetAnime,
        handleEditAnime,
        handleDeleteAnime,
        setModalShow,
        sincronizeAnimes,
    }

    return { state, stateUpdaters };
}


const newTodoId = (AnimeList: IAnimes[]) => {
    const idList = AnimeList.length ? AnimeList.map(anime => anime.id) : [0]
    const idMax = Math.max(...idList)
    return idMax + 1

}

export default useAnimes;