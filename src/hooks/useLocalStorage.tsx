import { useEffect, useReducer } from "react";

const useLocalStorage = <T,>(itemName: string, initialValue: T) => {

  const initialState = {
    sincronizedItem: true,
    error: false,
    loading: true,
    item: initialValue,
  };

  interface stateClassState {
    error: boolean,
    sincronizedItem: boolean,
    loading: boolean,
    item: T,
  }

  const reducerObject = (state: stateClassState, payload: any) => ({
    'ERROR': {
      ...state,
      error: true,
      loading: false,
      sincronizedItem: true,
    },
    'LOADING': {
      ...state,
      loading: true,
    },
    'SINCRONIZED': {
      ...state,
      loading: false,
      sincronizedItem: true,
      item: payload
    },
    'SAVE': {
      ...state,
      loading: false,
      item: payload
    },
    'SINCRONIZE': {
      ...state,
      loading: true,
      sincronizedItem: false,
    },
  });

  interface reducerState {
    "ERROR": stateClassState,
    "LOADING": stateClassState,
    "SINCRONIZED": stateClassState,
    "SAVE": stateClassState
    "SINCRONIZE": stateClassState
  }

  const reducer = (state: stateClassState, action: {
    [x: string]: any; type: keyof reducerState
  }): stateClassState => {
    const updatedState: reducerState = reducerObject(state, action.payload);
    return updatedState[action.type] || state;
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    sincronizedItem,
    item,
    loading,
    error
  } = state;

  const onSincronizedItem = (item: T | undefined) => dispatch({ type: "SINCRONIZED", payload: item })
  const onSincronize = () => dispatch({ type: "SINCRONIZE", payload: item })
  const onSave = (item: T | undefined) => dispatch({ type: "SAVE", payload: item })
  const onError = (error: unknown) => dispatch({ type: "ERROR", payload: error })

  useEffect(() => {
    let initialItem: T = initialValue;
    setTimeout(() => {
      try {
        const localStorageItem: string | null = localStorage.getItem(itemName);
        initialItem = localStorageItem ? (JSON.parse(localStorageItem) as T) : initialValue;
      } catch (error) {
        onError(error)
      }
      onSincronizedItem(initialItem)
    }, 800);
  }, [sincronizedItem]);

  const saveItem = (newItem: T) => {
    localStorage.setItem(itemName, JSON.stringify(newItem));
    onSave(newItem);
  };

  const sincronizeItem = () => {
    onSincronize()
  };

  return {
    item,
    saveItem,
    loading,
    error,
    sincronizeItem
  } as const;
};


export default useLocalStorage;
