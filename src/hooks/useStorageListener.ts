import { useState } from "react"


function useStorageListener(sincronize: () => void) {

    const [storageChange, setStorageChange] = useState(false);

    window.addEventListener('storage', (change) => {
        if (change.key === 'ANIMES_V2') {
            setStorageChange(true)
        }
    });

    const toggleShow = () => {
        sincronize();
        setStorageChange(false);
    }

    return {
        show: storageChange,
        toggleShow,
    }
}

export default useStorageListener;