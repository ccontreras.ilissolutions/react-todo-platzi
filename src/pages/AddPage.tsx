import { Col, Container, Row } from "react-bootstrap";
import AnimeForm from "../components/AnimeForm"
import useAnimes from "../hooks/useAnimes";

const AddPage = (): JSX.Element => {
    const {
        state,
        stateUpdaters
    } = useAnimes();

    const {
        searchedAnimes,
    } = state;

    const {
        handleAddAnime,
    } = stateUpdaters;

    return (
        <Container fluid className="min-vh-100 bg-form d-flex align-items-center">
            <Row className="g-0 w-100 mt-auto mb-5">
                <Col xs={12} lg={4}>
                    <AnimeForm
                        label="Ingresa un nuevo anime por ver"
                        submitText="Ingresar"
                        addEvent={handleAddAnime}
                        submitType="ADD"
                        searchedAnimes={searchedAnimes}
                    />
                </Col>
            </Row>
        </Container>
    );
};

export default AddPage;
