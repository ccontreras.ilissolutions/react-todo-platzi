import { HashRouter, Route, Routes } from "react-router-dom"
import HomePage from "./HomePage"
import AddPage from "./AddPage"
import EditPage from "./EditPage"
import NotFound from "./NotFound"

const App = (): JSX.Element => {

  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/add" element={<AddPage />} />
        <Route path="/edit/:id" element={<EditPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </HashRouter>
  )
}

export default App
