import { Col, Container, Row } from "react-bootstrap";
import AnimeForm from "../components/AnimeForm"
import useAnimes from "../hooks/useAnimes";

const EditPage = (): JSX.Element => {
    const {
        state,
        stateUpdaters
    } = useAnimes();

    const {
        searchedAnimes,
    } = state;

    const {
        handleEditAnime,
    } = stateUpdaters;

    return (
        <Container fluid className="min-vh-100 bg-edit d-flex align-items-center">
            <Row className="g-0 w-100 mt-auto mb-5">
                <Col lg={{ span: 4, offset: 8 }}>
                    <AnimeForm
                        label="Edita el anime ingresado"
                        submitText="Editar"
                        editEvent={handleEditAnime}
                        submitType="EDIT"
                        searchedAnimes={searchedAnimes}
                    />
                </Col>
            </Row>
        </Container>
    );
};

export default EditPage;
