import { Container, Row, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";

const NotFound = (): JSX.Element => {
    return (
        <Container fluid className="min-vh-100 bg-404 d-flex align-items-center">
            <Row className="w-100">
                <Col xs={10} className="d-flex justify-content-end me-auto">
                    <h1 className="text-404 text-white">404</h1>
                </Col>
                <Col xs={10} className="d-flex justify-content-end me-auto">
                    <h1 className="text-white">Página no encontrada</h1>
                </Col>
                <Col xs={10} className="d-flex justify-content-end me-auto">
                    <NavLink to={'/'} className={'text-link'}><h2>Ir a Home</h2></NavLink>
                </Col>
            </Row>
        </Container>
    );
};

export default NotFound;
