import { Button, Col, Container, ListGroup, Row } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';

import AnimeCounter from '../components/AnimeCounter'
import AnimeSearch from '../components/AnimeSearch'
import AnimeItem from '../components/AnimeItem'
import ItemLoarding from '../components/ItemLoarding';
import useAnimes from '../hooks/useAnimes';
import AnimeList from '../components/AnimeList';
import ItemError from '../components/ItemError';
import ItemEmpty from '../components/ItemEmpty';
import ChangeAlert from '../components/ChangeAlert';
import IAnimes from '../interfaces/IAnimes';

const HomePage = (): JSX.Element => {
  const {
    state,
    stateUpdaters
  } = useAnimes();

  const {
    searchedAnimes,
    loading,
    error,
    inputValue,
    completedAnimes,
    allAnimes,
  } = state;

  const {
    navigate,
    handleAddAnime,
    handleChangeAnimeStatus,
    handleDeleteAnime,
    handleChangeInput,
    setInputValue,
    sincronizeAnimes,
  } = stateUpdaters;

  return (
    <Container fluid className='min-vh-100 bg-main'>
      <Row className='g-0 d-flex align-items-center py-5 '>
        <Col xs={12} lg={5} className='rounded-4 bg-black position-relative vh-90 p-2'>
          <Col lg={8} className='mx-auto mt-1'>
            <AnimeSearch
              inputValue={inputValue}
              setInputValue={setInputValue}
              handleChangeInput={handleChangeInput}
              handleAddAnime={handleAddAnime}
              loading={loading}
            />
          </Col>

          <AnimeList
            error={error}
            onError={() => <ItemError />}
            loading={loading}
            onLoading={() => <ItemLoarding />}
            searchedAnimes={searchedAnimes}
            onEmpty={() => <ItemEmpty inputValue={inputValue} />}
            render={(anime: IAnimes) => (
              <Col lg={8} className='mx-auto' key={anime.id}>
                <ListGroup.Item>
                  <AnimeItem
                    id={anime.id}
                    text={anime.text}
                    completed={anime.completed}
                    onComplete={() => handleChangeAnimeStatus(anime.id)}
                    onDelete={() => handleDeleteAnime(anime.id)}
                  />
                </ListGroup.Item>
              </Col>
            )}
          />
          {allAnimes > 0 &&
            <div className='position-absolute bottom-0 end-0 pe-3 pb-3'>
              <AnimeCounter
                completed={completedAnimes}
                all={allAnimes}
              />
            </div>
          }
        </Col>
        <Col xs={12} lg={7} className='mx-auto text-center mt-auto mb-5'>
          <Button variant='warning' className='fw-bold' onClick={() => navigate('/add')}>
            <h3>Agregar Anime</h3>
          </Button>
        </Col>
      </Row>
      <ChangeAlert
        sincronize={sincronizeAnimes}
      />
    </Container>
  )
}

export default HomePage
