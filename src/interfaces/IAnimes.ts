interface IAnimes {
    id: number,
    text: string,
    completed: boolean,
}

export default IAnimes;