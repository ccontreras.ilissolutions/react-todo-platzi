import { Dispatch, SetStateAction, useEffect } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import useAnimes from '../hooks/useAnimes';

interface PropsAnimeSearch {
  inputValue: string,
  setInputValue: Dispatch<SetStateAction<string>>,
  handleChangeInput: (event: React.ChangeEvent<HTMLInputElement>) => void,
  handleAddAnime: (newAnime: string) => void
  loading: boolean;
}

const AnimeSearch = ({
  inputValue,
  handleChangeInput,
  handleAddAnime,
  loading,
}: PropsAnimeSearch): JSX.Element => {

  const {
    state,
    stateUpdaters
  } = useAnimes();

  const {
    params,
  } = state;

  const {
    navigate,
    setInputValue,
  } = stateUpdaters;

  console.log(params.toString())

  const defaultValue: string = !loading ? 'Filtrar o Agregar anime' : ''

  const handleKey = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter' || event.key === 'Tab') {
      event.preventDefault();
      handleAddAnime(inputValue);
      navigate(`/?find=${inputValue}`);
    }
  }

  useEffect(() => {
    setInputValue(params.toString())
  }, [params, setInputValue]);

  return (
    <InputGroup className='mb-3'>
      <Form.Control
        className='border-0 py-2'
        value={inputValue}
        placeholder={defaultValue}
        aria-label={defaultValue}
        onChange={handleChangeInput}
        onKeyDown={handleKey}
        disabled={loading}
      />
    </InputGroup>
  );
};

export default AnimeSearch;
