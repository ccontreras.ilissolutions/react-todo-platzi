import { PropsWithChildren } from "react";
import IAnimes from "../interfaces/IAnimes";

interface PropsAnimeList {
  error: boolean;
  onError: () => JSX.Element;
  loading: boolean;
  onLoading: () => JSX.Element;
  onEmpty: () => JSX.Element;
  render: (anime: IAnimes, index: number) => JSX.Element;
  searchedAnimes: IAnimes[];
}

const AnimeList = ({ error, onError, loading, onLoading, onEmpty, render, searchedAnimes }: PropsWithChildren<PropsAnimeList>): JSX.Element => {

  return (
    <>
      {error && onError()}
      {loading && onLoading()}
      {!loading && searchedAnimes?.length < 1 && onEmpty()}
      {!loading &&
        <div className='scrollable'>
          {searchedAnimes.map(render)}
        </div>
      }
    </>
  );
};

export default AnimeList;
