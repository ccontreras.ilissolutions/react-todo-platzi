import { useEffect, useRef, useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import IAnimes from "../interfaces/IAnimes";
import { useParams } from "react-router-dom";
import useAnimes from "../hooks/useAnimes";

interface propsAnimeForm {
    label: string,
    submitText: string,
    addEvent?: (value: string) => void;
    editEvent?: (id: number, value: string) => void
    submitType: 'ADD' | 'EDIT';
    searchedAnimes: IAnimes[];
}

const AnimeForm = ({
    label,
    submitText,
    addEvent,
    editEvent,
    submitType,
    searchedAnimes }: propsAnimeForm): JSX.Element => {

    const {
        state,
        stateUpdaters
    } = useAnimes();

    const {
        loading,
    } = state;

    const {
        navigate,
        location,
        handleGetAnime,
    } = stateUpdaters;

    const { id } = useParams();
    const idAnime: number = parseInt(id ?? '0')
    const animeData = handleGetAnime(idAnime);



    const [newAnimeValue, setNewAnimeValue] = useState<string>(location.state?.name ?? '');

    const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setNewAnimeValue(event.target.value)
        handleCompareAnime(event.target.value);
    }

    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        submitType === 'ADD' && addEvent && addEvent(newAnimeValue)
        submitType === 'EDIT' && editEvent && idAnime > 0 && editEvent(idAnime, newAnimeValue)
        navigate('/')
    }

    const handleNavigate = () => {
        navigate('/')
    }

    const inputRef = useRef<HTMLInputElement | null>(null);

    const [inputError, setInputError] = useState<string>('');
    const [buttonState, setButtonState] = useState<string>('disabled')

    const handleCompareAnime = (animeName: string) => {
        switch (submitType) {
            case 'ADD': {
                if (searchedAnimes.some(anime => anime.text.toLowerCase() === animeName.toLowerCase().trim())) {
                    setInputError('Ya este anime existe');
                    setButtonState('disabled')
                } else if (inputRef.current?.value.trim() === '') {
                    setInputError('');
                    setButtonState('disabled')
                } else {
                    setInputError('');
                    setButtonState('')
                }
                break;
            }
            case 'EDIT': {
                const anime: IAnimes | undefined = searchedAnimes.find(anime => anime.text.toLowerCase() === animeName.toLowerCase().trim());
                if (anime && anime.id !== idAnime) {
                    setInputError('Ya este anime existe en otro registro');
                    setButtonState('disabled')
                } else if (inputRef.current?.value.trim() === '') {
                    setInputError('');
                    setButtonState('disabled')
                } else {
                    setInputError('');
                    setButtonState('')
                }
                break;
            }
            default: {
                setInputError('');
                setButtonState('')
                break;
            }
        }
    }

    useEffect(() => {
        // Cuando el componente se monta, enfoca automáticamente el input
        if (inputRef.current) {
            inputRef.current.focus();
        }
    }, []);

    useEffect(() => {
        !location.state?.name && submitType === 'EDIT' && setButtonState('disabled')
        !location.state?.name && submitType === 'EDIT' && setNewAnimeValue('Cargando....')
        !loading && submitType === 'EDIT' && setNewAnimeValue(animeData?.text ?? '')
        !loading && !animeData && submitType === 'EDIT' && navigate('/404')
        !loading && submitType === 'EDIT' && setButtonState('')
    }, [animeData, loading, location.state?.name, navigate, submitType]);

    return (
        <Form onSubmit={onSubmit} className="bg-dark p-4 rounded rounded-4">
            <Form.Group className="mb-3">
                <Form.Label><h2 className="text-white">{label}</h2></Form.Label>
                <Form.Control
                    ref={inputRef}
                    type="text"
                    placeholder="Escribelo acá"
                    value={newAnimeValue}
                    onChange={onChange}
                />
                <Form.Text className="text-warning">
                    {inputError}
                </Form.Text>
            </Form.Group>
            <Row>
                <Col xs={6}>
                    <Button variant="light" type="submit" className={`w-100 ${buttonState}`}>
                        {submitText}
                    </Button>
                </Col>
                <Col xs={6}>
                    <Button variant="secondary" type="submit" className={`w-100`} onClick={handleNavigate}>
                        Regresar
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default AnimeForm;
