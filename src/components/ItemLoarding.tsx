import { Col, ListGroup } from "react-bootstrap";

const ItemLoading = (): JSX.Element => {
  return (
    <Col lg={8} className='mx-auto'>
      {[...Array(10)].map((_, index) => (
        <ListGroup.Item className='d-flex rounded-3 gradient-bg-animation my-1' key={index} />
      ))}
    </Col>

  );
};

export default ItemLoading;
