import { useState } from 'react';

import { Col, Form } from 'react-bootstrap';

import { RiChatDeleteFill, RiEdit2Fill } from "react-icons/ri";
import useAnimes from '../hooks/useAnimes';



interface propsAnimeItem {
  completed: boolean;
  id: number;
  text: string;
  onComplete: (text: string) => void;
  onDelete: (text: string) => void;
}

const AnimeItem = ({
  completed,
  id,
  text,
  onComplete,
  onDelete,
}: propsAnimeItem): JSX.Element => {

  const {
    stateUpdaters
  } = useAnimes();

  const {
    navigate,
  } = stateUpdaters;

  const [checkActive, setCheckActive] = useState<boolean>(completed);

  const handleChangeSwitch = (): void => {
    setCheckActive(!checkActive)
    onComplete(text)
  }

  return (
    <Form className='d-flex justify-content-between align-items-center bg-primary-1 px-2 py-3 border border-primary-1 rounded-3 my-1'>
      <Form.Switch
        defaultChecked={checkActive}
        id='custom-switch'
        onClick={handleChangeSwitch}
      />
      <Col className='d-flex justify-content-between'>
        <Col>
          <span className={`fw-bold ${checkActive && 'text-decoration-line-through text-light'}`}>{text}</span>
        </Col>
        <Col className='text-end'>
          <span className='px-2' onClick={() => navigate(`/edit/${id}`, { state: { name: text } })}><RiEdit2Fill color='#000000' size={24} /></span>
          <span className='px-2' onClick={() => onDelete(text)}><RiChatDeleteFill color='#cb3234' size={24} /></span>
        </Col>
      </Col>
    </Form>
  );
};

export default AnimeItem;
