import { Button, Modal } from "react-bootstrap";
import useStorageListener from "../hooks/useStorageListener";

interface PropsChangeAlert {
    sincronize: () => void;
}

const ChangeAlert = ({ sincronize }: PropsChangeAlert): JSX.Element => {

    const { show, toggleShow } = useStorageListener(sincronize);

    return (
        show ?
            <Modal
                size="sm"
                show={show}
                onHide={toggleShow}
                aria-labelledby="example-modal-sizes-title-sm"
                centered
            >
                <Modal.Header closeButton className="bg-primary-1 text-light border border-primary-1">
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Alerta
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="bg-black text-light border border-primary-1">
                    <div className="text-center my-2">
                        <span >Hubieron Cambios y esta data no esta actualizada, al cerrar la modal se actualizaran los cambios</span>
                    </div>
                    <Button className="bg-primary-1 w-100" onClick={toggleShow} > Actualizar manualmente</Button>
                </Modal.Body>
            </Modal>
            :
            <></>
    );
};

export default ChangeAlert;
