import { Col, ListGroup } from "react-bootstrap";

const ItemError = (): JSX.Element => {
    return (
        <ListGroup.Item className='d-flex'>
            <Col lg={8} className='mx-auto my-1 mt-2 px-1 py-3 rounded-3 border border-dashed border-danger'>
                <div className='text-danger text-center'>Tuvimos problemas en cargar tus pendientes</div>
            </Col>
        </ListGroup.Item>
    );
};

export default ItemError;
