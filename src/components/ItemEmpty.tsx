import { PropsWithChildren } from "react";
import { Col, ListGroup } from "react-bootstrap";

interface PropsItemEmpty {
    inputValue: string
}

const ItemEmpty = ({ inputValue }: PropsWithChildren<PropsItemEmpty>): JSX.Element => {
    return (
        <ListGroup.Item className='d-flex'>
            <Col lg={8} className='mx-auto my-1 mt-2 px-1 py-3 rounded-3 border border-dashed border-light' onClick={() => inputValue === '' && /*setModalShow(true) :*/ null}>
                {inputValue !== '' ?
                    <>
                        <span className='text-light text-center px-2'>{'No conseguimos nada en la busqueda relacionado al termino '}</span><span className="fw-bold text-warning">{inputValue}</span><span className='text-light text-center px-2'>{', presiona Enter al terminar de escribir el Anime y agregalo como nuevo'}</span>
                    </>
                    :
                    <span className='text-light text-center px-2'>{'Aún no tienes nada en esta lista, agrega un nuevo anime'}</span>
                }
            </Col>
        </ListGroup.Item>
    );
};

export default ItemEmpty;
