interface PropsAnimeCounter {
  completed: number,
  all: number
}

const AnimeCounter = ({
  completed,
  all,
}: PropsAnimeCounter): JSX.Element => {
  return (
    <span className='fw-light h5 fw-bold text-light'>{`${completed}/${all}`}</span>
  );
};

export default AnimeCounter;
